import java.io.File;
import java.io.FileNotFoundException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;

public class IBeforeE {
	static ArrayList<String> words;
	
	public static void main(String[] args) throws FileNotFoundException {
		  words = new ArrayList<String>();
		  @SuppressWarnings("resource")
		  Scanner sc = new Scanner(new File("test2.txt"));
		  while(sc.hasNext()) {
		    words.add(sc.next());
		  }
		  System.out.println(toPercent(prcntFollowsRules(totalWords(words))));
		  // 1.) Total words that have ie or ei
		  // 2.) Check if those instances follow or don't follow the rule
		  //		      ?ei = doesn't follow
		  //      		cei = follows
		  //      ?ie = follows
		  //      cie = doesn't follow
		  // where ? does not equal c
		  // final output: print the percentage
	  }
	
	  /**
	   * @param list
	   * @return A list of words that contain 'ie' or 'ei'
	   */
	  public static ArrayList<String> totalWords(ArrayList<String> list) {
	    ArrayList<String> wordsIE = new ArrayList<String>();
	    for(String s : list) {
	      for(int i = 0; i < s.length() - 1; i++) {
	        if(s.charAt(i) == 'e' && s.charAt(i+1) == 'i'
	           || s.charAt(i) == 'i' && s.charAt(i+1) == 'e') {
	          wordsIE.add(s);
	          break;
	        }
	      }
	    }
	    return wordsIE;
	  }
	
	  /**
	 * @param list
	 * @return Percentage of words in the list that follow the rule
	 */
	  public static double prcntFollowsRules(ArrayList<String> list) {
	    float total = list.size();
	    float countFollowsRules = listFollowsRules(list).size();
	    return countFollowsRules/total;
	  }
	
	  /**
	   * @param list
	   * @return A list of the words that follow the rule from another list
	   */
	  public static ArrayList<String> listFollowsRules(ArrayList<String> list) {
	    ArrayList<String> res = new ArrayList<String>();
	    for(String s : list) {
	      for(int i = 1; i < s.length() - 1; i++) {
	        if(s.charAt(i-1) != 'c' && s.charAt(i) == 'i' && s.charAt(i+1) == 'e'
	           || (s.charAt(i-1) == 'c' && s.charAt(i) == 'e' && s.charAt(i+1) == 'i')) {
	          res.add(s);
	          break;
	        }
	      }
	    }
	    return res;
	  }

	  public static String toPercent(double n) {
	    DecimalFormat format = new DecimalFormat("###.000");
	    return format.format(n * 100) + "%";
	  }
}